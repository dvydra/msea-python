# This is a guess the number game. 
# from Invent Your Own COmputer Games with Python, 2nd Ed. by Al Sweigart
# Python3.2 or greater
import random

MAX_GUESSES = 6

guessesTaken = 0

print('Hello! What is your name?')
myName = input()

numberToGuess = random.randint(1, 20)

print('Well, ' + myName + ', I am thinking of a number between 1 and 20.')

while guessesTaken < MAX_GUESSES:

    guessesTaken += 1
    
    print('Take a guess.') # There are four spaces in front of print.
    guess = int(input())
  
    if guess < numberToGuess:

        print('Your guess is too low.') # There are eight spaces in front of print.

    if guess > numberToGuess:

        print('Your guess is too high.')

    if guess == numberToGuess:
        print('Good job, ' + myName + '! You guessed my number in ' + str(guessesTaken) + ' guesses!')
        break
  

if guessesTaken == MAX_GUESSES:
    print('Nope. The number I was thinking of was ' + str(numberToGuess))