class Ship(object):
    def __init__(self,size,char_ind):
        self._size = size
        self.char_ind = char_ind
    def size(self):
        return self._size

    def set_ori(self,ori):
        if not (ori == 'h' or ori == 'v'):
            raise Exception ("Orientation must be h or v")
        self._ori = ori

    def ori(self):
        return self._ori

    def set_cord(self,cord):
        self._cord = cord

    def coordinate(self):
        return self._cord

class Destroyer(Ship):
    def __init__(self):
        Ship.__init__(self,2,'D')

class Cruiser(Ship):
    def __init__(self):
        Ship.__init__(self,3,'C')

class Submarine(Ship):
    def __init__(self):
        Ship.__init__(self,3,'S')

class Battleship(Ship):
    def __init__(self):
        Ship.__init__(self,4,'B')

class AircraftCarrier(Ship):
    def __init__(self):
        Ship.__init__(self,5,'A')
