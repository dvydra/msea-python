from board import *
from player import *
from ship import *


class Maxim(Player):

    def __init__(self):
        Player.__init__(self,"Maxim")

    def place_ships(self):
        ac = AircraftCarrier()
        ac.set_ori('h')
        self.board.add_ship(ac,1,1)

        bs = Battleship()
        bs.set_ori("v")
        self.board.add_ship(bs,2,3)

        cr = Cruiser()
        cr.set_ori("h")
        self.board.add_ship(cr,4,5)

    def go(self):
        if self.board.fire(1,1) == "hit":
            print "hit"

