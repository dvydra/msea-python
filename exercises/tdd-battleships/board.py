class Board(object):

    def __init__(self,size):
        self._size = size
        self.rows = []
        for i in range(0,self._size):
            row = []
            for j in range(0,self._size):
               row.append((None,None))
            self.rows.append(row)


    def size(self):
        return self._size

    def add_ship(self,ship,x,y):
        if ship.ori() == 'h':
            for i in range(0, ship.size()):
                self.rows[x+i-1][y-1] = (ship,None)
        elif ship.ori() == 'v':
            for i in range(0, ship.size()):
                self.rows[x-1][y+i-1] = (ship,None)
        ship.set_cord((x,y))

    def fire(self,x,y):
        if self.at(x,y)[0] == None:
            self.rows[x-1][y-1] = (None,'miss')
            return 'miss'
        else:
            ship = self.at(x,y)[0]
            self.rows[x-1][y-1] = (ship,'hit')
            return 'hit'

    def at(self,x,y):
        return self.rows[x-1][y-1]




