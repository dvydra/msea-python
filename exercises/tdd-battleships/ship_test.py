import unittest
from ship import *

class ShipTest(unittest.TestCase):

    def setUp(self):
        self.ac = AircraftCarrier()
        self.ac.set_ori('h')
        self.ac_v = AircraftCarrier()
        self.ac_v.set_ori('v')


    def test_create_ship(self):
        self.assertEqual(self.ac.size(),5)

    def test_wrong_ori(self):
        self.assertRaises(Exception,self.ac.set_ori,'w')

    def test_set_orientation(self):
        self.ac.set_ori('h')
        self.assertEqual(self.ac.ori(),'h')


if __name__ == '__main__':
    unittest.main()