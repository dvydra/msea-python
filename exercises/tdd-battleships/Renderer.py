from board import *
from ship import *
from player import *

class Render(object):

    def __init__(self, player):
        self.player = player


    def print_board(self):
        print self.player.name
        for y in range(10,0,-1):
            self.print_row(y)
        self.print_x_axis()

    def print_x_axis(self):
        print " ",
        for i in range(1,11):
            print " " + str(i),
        print ' '

    def print_row(self,y):
        print str(y) + " ",
        for x in range(1,11):
            cell = self.player.board.at(x,y)
            ship = cell[0]
            hit = cell[1]
            if ship != None:
                if hit == 'hit':
                    print "X ",
                else:
                    print ship.char_ind + " ",
            else:
                print ' ',
        print " "



if __name__ == '__main__':

    board = Board(10)

    #===========================================================================
    # ac = Ship(5)
    # ac.set_ori('h')
    # board.add_ship(ac, 1,1)
    #===========================================================================

    submarine = Submarine()
    submarine.set_ori('v')
    board.add_ship(submarine,3,6)

    renderer = Render(board)
    renderer.print_board()