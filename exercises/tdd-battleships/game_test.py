import unittest
from game import *
from player import *
from board import *
from max import *
from dima import *

class GameTest(unittest.TestCase):
    def setUp(self):
        self.game = Game(Maxim(),Dima())

    def test_run(self):
        self.game.run()

if __name__ == '__main__':
    unittest.main()